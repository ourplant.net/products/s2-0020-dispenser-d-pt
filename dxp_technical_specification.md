The S2-0020 Dispenser D-PT has the following technical specifications.

## Technical information
| Dimensions in mm (W x D x H) | 49 x 310 x 516     |
| :--------------------------- | ------------------ |
| Weight in kg                 | 4.8                |
| Movement range in Z in mm    | 150                |
| Cartridge holder SEMCO       | 10 cc, 30 cc, 5 cc |