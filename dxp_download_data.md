Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/01_operating_manual/S2-0020_E_BA_Dispenser.pdf), [en](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/01_operating_manual/S2-0020_E_OM_Dispenser.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/02_assembly_drawing/s2-0020_C_ZNB_D-PT_Basis.pdf)|
|circuit diagram            |[without tempering](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/03_circuit_diagram/S2-0020_C_EPLAN_Dispenser.pdf), [with tempering](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/03_circuit_diagram/S2-0020_A_EPLAN_Dispenser_mit_Temperierung.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/04_maintenance_instructions/S2-0020_B_WA_Dispenser.pdf), [en](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/04_maintenance_instructions/S2-0020_B_MI_Dispenser.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0020-dispenser-d-pt/-/raw/main/05_spare_parts/S2-0020_A1_EVL_Dispenser.pdf), [en](https://gitlab.com/ourplant.net/products/dispenss2-0020-dispenser-d-pt/-/raw/main/05_spare_parts/S2-0020_A1_SWP_Dispenser.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
